


CREATE DATABASE blog_db;

use blog_db;

CREATE TABLE users(
	id INT NOT NULL AUTO_INCREMENT,
	email VARCHAR(100) NOT NULL,
	password VARCHAR(300) NOT NULL,
	date_created DATETIME NOT NULL,
	PRIMARY KEY (id)
);



CREATE TABLE posts(
	id INT NOT NULL AUTO_INCREMENT,
	title VARCHAR(500) NOT NULL,
	content VARCHAR(5000) NOT NULL,
	date_posted DATETIME NOT NULL,
	PRIMARY KEY (id)
);


INSERT INTO posts(title,content,date_posted) VALUES ('First Code', 'Hello World!', CURRENT_TIMESTAMP);
INSERT INTO posts(title,content,date_posted) VALUES ('Second Code', 'Hello Earth!', CURRENT_TIMESTAMP);
INSERT INTO posts(title,content,date_posted) VALUES ('Third Code', 'Welcome to Mars!', CURRENT_TIMESTAMP);
INSERT INTO posts(title,content,date_posted) VALUES ('Fourth Code', 'Bye bye solar system!', CURRENT_TIMESTAMP);



INSERT INTO users(email,password,date_created) VALUES ('johnsmite@gmail.com', 'passwordA', CURRENT_TIMESTAMP);
INSERT INTO users(email,password,date_created) VALUES ('juandelacruz@gmail.com', 'passwordB', CURRENT_TIMESTAMP);
INSERT INTO users(email,password,date_created) VALUES ('janesmith@gmail.com', 'passwordC', CURRENT_TIMESTAMP);
INSERT INTO users(email,password,date_created) VALUES ('mariadelacruz@gmail.com', 'passwordD', CURRENT_TIMESTAMP);
INSERT INTO users(email,password,date_created) VALUES ('johndoe@gmail.com', 'passwordE', CURRENT_TIMESTAMP);


SELECT * FROM users WHERE id=1;
SELECT email,date_created FROM users;


UPDATE posts SET content = 'Hello people of the earth!' WHERE content = 'Hello World!';


DELETE FROM users WHERE email = 'johndoe@gmail.com';